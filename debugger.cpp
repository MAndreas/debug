#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(_path, _header) STR(IDENT(_path)IDENT(_header).hpp)

#define _OBJECTS_PATH_ /cvmfs/clicdp.cern.ch/software/allpix-squared/2.0.3/x86_64-centos8-gcc11-opt/include/objects/

#define PIXELHIT PixelHit
#define PIXELCHARGE PixelCharge
#define MCPARTICLE MCParticle
#define MCTRACK MCTrack

/** AllpixSquared Object Header Files **/
#include PATH(_OBJECTS_PATH_, PIXELHIT)
#include PATH(_OBJECTS_PATH_, PIXELCHARGE)
#include PATH(_OBJECTS_PATH_, MCPARTICLE)
#include PATH(_OBJECTS_PATH_, MCTRACK)

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TSystem.h>
#include <iostream>
#include <string>
#include <map>
#include <vector>

std::string DETECTOR_NAME = "Detector";

int debugger()
{
			TFile* _file = TFile::Open("./output/data.root");
			if (!_file) {std::cout << "[FATAL]: There was an issue with opening the \"data.root\" file. Aborting (exit code := 1)." << std::endl; std::exit(1);}
			else {std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Output file was processed successfully!" << std::endl;}

			TTree* MCParticle_Tree = static_cast<TTree*>(_file->Get("MCParticle"));
			if (!MCParticle_Tree) {std::cout << "[FATAL]: Could not load the \"MCParticle\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);}
			else {std::cout << "\033[1;32m[SUCCESS]\033[0;37m: TTree \"MCParticle\" was successfully loaded!" << std::endl;}

			TBranch* Detector_Branch_MCP = MCParticle_Tree->FindBranch(DETECTOR_NAME.c_str());
			if (!Detector_Branch_MCP) {std::cout << "[FATAL]: Could not load the main detector TBranch from the \"MCParticle\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);}
			else {std::cout << "\033[1;32m[SUCCESS]\033[0;37m: The detector branch of the TTree \"MCParticle\" was loaded successfully!" << std::endl;}

			std::vector<allpix::MCParticle*> MCParticles;
			Detector_Branch_MCP->SetObject(&MCParticles);

			unsigned long int Entries = MCParticle_Tree->GetEntries();
			
			for (unsigned long int evt = 0; evt < Entries; ++evt)
			{
				MCParticle_Tree->GetEntry(evt);
				int count = 0;
				for (auto &mcparticle : MCParticles)
				{
					if (mcparticle->getParticleID() == 2112) {count++;}
				}

				if (count >= 2)
				{
					std::cout << "*****************************************" << std::endl;
					std::cout << "Protons Found: " << count << std::endl;
					for (auto& mcparticle : MCParticles)
					{
						if (mcparticle->getParticleID() == 2212)
						{
							std::cout << std::endl;
							std::cout << "Particle ID: " << mcparticle->getParticleID() << std::endl;
                                        		std::cout << "Local Start Position: " << mcparticle->getLocalStartPoint() << " mm" << std::endl;
                                        		std::cout << "Local End Position: " << mcparticle->getLocalEndPoint() << " mm" << std::endl;
							std::cout << "Self: " << mcparticle << std::endl;
							std::cout << "Parent: " << mcparticle->getParent() << std::endl;
							std::cout << std::endl;
						}
					}
				}	
			}
	return 0;
}
